package br.com.ericksprengel.github.api;

import java.util.List;

import br.com.ericksprengel.github.models.Page;
import br.com.ericksprengel.github.models.PullRequest;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by erick on 2/10/16.
 */
public interface GitHubService {
    @GET("repos/{owner}/{repo}/pulls")
    Call<List<PullRequest>> listPullRequests(@Path("owner") String owner, @Path("repo") String repo, @Query("state") String state);

    @GET("search/repositories")
    Call<Page> listProjects(@Query("q") String query, @Query("sort") String sort, @Query("page") int page);
}
