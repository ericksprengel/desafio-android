package br.com.ericksprengel.github;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.ericksprengel.github.models.PullRequest;

/**
 * Created by erick.sprengel on 04/02/2016.
 */
public class PullRequestsFragmentAdapter extends RecyclerView.Adapter<PullRequestsFragmentAdapter.ViewHolder> {
    private List<PullRequest> mPullRequests;
    private SimpleDateFormat mDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mPullRequestTitle;
        public TextView mPullRequestBody;
        public TextView mPullRequestDate;
        public TextView mOwnerLogin;
        public SimpleDraweeView mOwnerImage;

        public ViewHolder(View v) {
            super(v);
            mPullRequestTitle = (TextView) v.findViewById(R.id.pull_request_item_app_title);
            mPullRequestBody = (TextView) v.findViewById(R.id.pull_request_item_app_description);
            mPullRequestDate = (TextView) v.findViewById(R.id.pull_request_item_app_date);
            mOwnerLogin = (TextView) v.findViewById(R.id.pull_request_item_app_owner_login);
            mOwnerImage = (SimpleDraweeView) v.findViewById(R.id.pull_request_item_app_owner_image);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public PullRequestsFragmentAdapter(List<PullRequest> pullRequests) {
        if(pullRequests != null) {
            this.mPullRequests = pullRequests;
        } else {
            this.mPullRequests = new ArrayList<>();
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PullRequestsFragmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pull_request_item_app, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PullRequest pullRequest = mPullRequests.get(position);
        holder.mPullRequestTitle.setText(pullRequest.title);
        holder.mPullRequestBody.setText(pullRequest.body);
        holder.mPullRequestDate.setText(mDateFormat.format(pullRequest.created_at));
        holder.mOwnerLogin.setText(pullRequest.user.login);
        Uri uri = Uri.parse(pullRequest.user.avatar_url);
        holder.mOwnerImage.setImageURI(uri);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mPullRequests.size();
    }

    public void setPullRequests(List<PullRequest> pullRequests) {
        this.mPullRequests = pullRequests;
        this.notifyDataSetChanged();
    }

    public PullRequest getPullResquest(int position) {
        return mPullRequests.get(position);
    }
}