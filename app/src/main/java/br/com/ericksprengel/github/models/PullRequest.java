package br.com.ericksprengel.github.models;

import java.util.Date;

/**
 * Created by erick.sprengel on 04/02/2016.
 */
public class PullRequest {

    public static final String STATE_OPENED = "open";
    public static final String STATE_CLOSED = "closed";
    public static final String STATE_ALL = "all";

    public long id;
    public String title;
    public String body;
    public String html_url;
    public Date created_at;
    public User user;
    public String state;
}
