package br.com.ericksprengel.github;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.ericksprengel.github.api.GitHubService;
import br.com.ericksprengel.github.api.GitHubServiceBuilder;
import br.com.ericksprengel.github.models.Page;
import br.com.ericksprengel.github.models.Project;
import br.com.ericksprengel.github.utils.Configs;
import br.com.ericksprengel.github.utils.RecyclerItemClickListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A placeholder fragment containing a simple view.
 */
public class ProjectsFragment extends BaseAppFragment implements ProjectFragmentAdapter.EndlessProjectAdapterListener, Callback<Page> {
    private ProjectFragmentAdapter mAdapter;
    private int mCurrentPage;
    private long mTotalCount;
    private boolean mLoading;


    private View.OnClickListener mErrorListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            requestMoreProjects();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        List<Project> projects = getAppActivity().getDataFragment().getProjects();
        mCurrentPage = getAppActivity().getDataFragment().getProjectsPage();

        View mainView = inflater.inflate(R.layout.project_list_app, container, false);
        RecyclerView mRecyclerView = (RecyclerView) mainView.findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Project project = mAdapter.getProject(position);
                        openPullRequests(project);
                    }
                })
        );

        // use a linear layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ProjectFragmentAdapter(projects, this);
        mRecyclerView.setAdapter(mAdapter);

        if(mCurrentPage == 0) {
            requestMoreProjects();
        }

        return mainView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getActionBar().setTitle(R.string.app_name);
    }

    private void requestMoreProjects() {
        mLoading = true;
        mCurrentPage++;
        GitHubService service = GitHubServiceBuilder.build();
        Call<Page> c = service.listProjects("language:Java", "stars", mCurrentPage);
        c.enqueue(this);
    }

    private void openPullRequests(Project project) {
        Bundle bundle = new Bundle();
        bundle.putLong("project_id", project.id);
        getAppActivity().goToFragment(AppActivity.FRAGMENT_PULL_REQUESTS, bundle, true);
    }

    @Override
    public void onLoadMore() {
        if(mLoading || mTotalCount == mAdapter.getItemCount()) {
            return;
        }
        requestMoreProjects();
    }

    @Override
    public void onResponse(Call<Page> call, Response<Page> response) {
        if(getAppActivity() == null) {
            return;
        }

        mLoading = false;
        if(!response.isSuccess()) {
            mCurrentPage--;
            showError("Erro interno. (" + response.code() + ")", mErrorListener, "Tentar Novamente");
            Log.e(Configs.LOG_TAG, "Erro interno.! Page " + mCurrentPage);
            return;
        }
        Page page = response.body();
        this.mTotalCount = page.total_count;
        mAdapter.addPage(page);
        this.getAppActivity().getDataFragment().setProjects(mAdapter.getProjects(), mCurrentPage);
        showMainFragment();
        Log.d(Configs.LOG_TAG, "More items! Page " + mCurrentPage);
    }

    @Override
    public void onFailure(Call<Page> call, Throwable t) {
        if(getAppActivity() == null) {
            return;
        }

        showError("Erro de conexão.", mErrorListener, "Tentar Novamente");
    }
}
