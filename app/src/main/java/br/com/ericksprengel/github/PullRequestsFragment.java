package br.com.ericksprengel.github;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.com.ericksprengel.github.api.GitHubService;
import br.com.ericksprengel.github.api.GitHubServiceBuilder;
import br.com.ericksprengel.github.models.Project;
import br.com.ericksprengel.github.models.PullRequest;
import br.com.ericksprengel.github.utils.RecyclerItemClickListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A placeholder fragment containing a simple view.
 */
public class PullRequestsFragment extends BaseAppFragment implements Callback<List<PullRequest>> {
    private TextView mOpenedPullRequestsView;
    private TextView mClosedPullRequestsView;

    private PullRequestsFragmentAdapter mAdapter;

    private Project mProject;
    private View.OnClickListener mErrorListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            requestPullRequests();
        }
    };
    private DialogInterface.OnClickListener mCancelListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            getAppActivity().backFragment();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.pull_request_list_app, container, false);

        mProject = getAppActivity().getDataFragment().getProject(this.getArguments().getLong("project_id"));
        RecyclerView mRecyclerView = (RecyclerView) mainView.findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        PullRequest pullRequest = mAdapter.getPullResquest(position);
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(pullRequest.html_url));
                        startActivity(intent);
                    }
                })
        );

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new PullRequestsFragmentAdapter(mProject.getPullRequests());
        mRecyclerView.setAdapter(mAdapter);

        mOpenedPullRequestsView = (TextView) mainView.findViewById(R.id.pull_request_item_app_opened_pull_requests);
        mClosedPullRequestsView = (TextView) mainView.findViewById(R.id.pull_request_item_app_closed_pull_requests);

        if(mProject.getPullRequests() == null) {
            requestPullRequests();
        }
        //else: already loaded

        updateViews();



        return mainView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getActionBar().setTitle(mProject.full_name);
    }

    private void requestPullRequests() {
        GitHubService service = GitHubServiceBuilder.build();
        Call<List<PullRequest>> c = service.listPullRequests(mProject.owner.login, mProject.name, PullRequest.STATE_ALL);
        c.enqueue(this);
    }

    private void updateViews() {
        if(mProject.opened_pull_requests_count != null) {
            mOpenedPullRequestsView.setText(String.format("%s opened", mProject.opened_pull_requests_count));
        }
        if(mProject.closed_pull_requests_count != null) {
            mClosedPullRequestsView.setText(String.format("%s closed", mProject.closed_pull_requests_count));
        }

    }

    @Override
    public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
        if(getAppActivity() == null) {
            return;
        }

        if(!response.isSuccess()) {
            showError("Erro interno. (" + response.code() + ")", PullRequestsFragment.this.mErrorListener, "Tentar Novamente");
            return;
        }
        List<PullRequest> pullRequests = response.body();
        mProject.setPullRequests(pullRequests);
        mAdapter.setPullRequests(pullRequests);
        updateViews();
        showMainFragment();
    }

    @Override
    public void onFailure(Call<List<PullRequest>> call, Throwable t) {
        if (getAppActivity() == null) {
            return;
        }

        showError("Erro de conexão.", mErrorListener, "Tentar Novamente");
    }
}
