package br.com.ericksprengel.github.models;

import java.io.Serializable;

/**
 * Created by erick on 2/6/16.
 */
public class User implements Serializable {

    public long id;
    public String login;
    public String avatar_url;

}
