package br.com.ericksprengel.github.models;

import java.util.ArrayList;

/**
 * Created by erick on 2/6/16.
 */
public class Page {

    public long total_count;
    public boolean incomplete_results;
    public ArrayList<Project> items;
}
