package br.com.ericksprengel.github.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by erick.sprengel on 04/02/2016.
 */
public class Project implements Serializable {

    public long id;
    public String name;
    public String full_name;
    public String description;
    public User owner;
    public long forks_count;
    public long stargazers_count;
    public String pulls_url;
    public Integer opened_pull_requests_count = null;
    public Integer closed_pull_requests_count = null;
    private List<PullRequest> pullRequests;


    public void setPullRequests(List<PullRequest> pullRequests) {
        opened_pull_requests_count = 0;
        closed_pull_requests_count = 0;
        for(int i = 0; i < pullRequests.size(); i++) {
            PullRequest pullRequest = pullRequests.get(i);
            if(pullRequest.state.equals(PullRequest.STATE_OPENED)) {
                opened_pull_requests_count++;
            } else if(pullRequest.state.equals(PullRequest.STATE_CLOSED)) {
                closed_pull_requests_count++;
            }
        }
        this.pullRequests = pullRequests;
    }

    public List<PullRequest> getPullRequests() {
        return this.pullRequests;
    }
}
