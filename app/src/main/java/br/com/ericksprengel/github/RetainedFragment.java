package br.com.ericksprengel.github;

import android.app.Fragment;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.ericksprengel.github.models.Project;
import br.com.ericksprengel.github.models.PullRequest;

/**
 * Created by erick on 2/7/16.
 */
public class RetainedFragment extends Fragment {

    private List<Project> mProjects = new ArrayList<>();
    private int mProjectsPage = 0;


    // this method is only called once for this fragment
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // retain this fragment
        setRetainInstance(true);
    }

    public void setProjects(List<Project> projects, int page) {
        this.mProjects = projects;
        this.mProjectsPage = page;
    }

    public List<Project> getProjects() {
        return mProjects;
    }

    public Project getProject(long id) {
        for(int i = 0; i < mProjects.size(); i++) {
            Project project = mProjects.get(i);
            if(project.id == id) {
                return project;
            }
        }
        throw new IllegalArgumentException("Invalid project id");
    }

    public int getProjectsPage() {
        return mProjectsPage;
    }
}
