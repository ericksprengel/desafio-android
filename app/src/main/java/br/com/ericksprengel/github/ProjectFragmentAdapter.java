package br.com.ericksprengel.github;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import br.com.ericksprengel.github.models.Page;
import br.com.ericksprengel.github.models.Project;

/**
 * Created by erick.sprengel on 04/02/2016.
 */
public class ProjectFragmentAdapter  extends RecyclerView.Adapter<ProjectFragmentAdapter.ViewHolder> {
    private final EndlessProjectAdapterListener mEndlessListener;
    private List<Project> mProjects;

    public Project getProject(int position) {
        return mProjects.get(position);
    }

    public List<Project> getProjects() {
        return mProjects;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mProjectName;
        public TextView mProjectDescription;
        public TextView mProjectForks;
        public TextView mProjectStars;
        public TextView mOwnerLogin;
        SimpleDraweeView mOwnerImage;
        public ViewHolder(View v) {
            super(v);
            mProjectName = (TextView) v.findViewById(R.id.project_item_app_project_name);
            mProjectDescription = (TextView) v.findViewById(R.id.project_item_app_project_description);
            mProjectForks = (TextView) v.findViewById(R.id.project_item_app_project_forks);
            mProjectStars = (TextView) v.findViewById(R.id.project_item_app_project_stars);
            mOwnerLogin = (TextView) v.findViewById(R.id.project_item_app_owner_login);
            mOwnerImage = (SimpleDraweeView) v.findViewById(R.id.project_item_app_owner_image);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ProjectFragmentAdapter(List<Project> projects, EndlessProjectAdapterListener endlessListener) {
        this.mProjects = projects;
        this.mEndlessListener = endlessListener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ProjectFragmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.project_item_app, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Project project = mProjects.get(position);
        holder.mProjectName.setText(project.name);
        holder.mProjectDescription.setText(project.description);
        holder.mProjectForks.setText(String.valueOf(project.forks_count));
        holder.mProjectStars.setText(String.valueOf(project.stargazers_count));
        holder.mOwnerLogin.setText(project.owner.login);
        Uri uri = Uri.parse(project.owner.avatar_url);
        holder.mOwnerImage.setImageURI(uri);

        if(position == mProjects.size() - 3) {
            mEndlessListener.onLoadMore();
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mProjects.size();
    }

    public void addPage(Page page) {
        mProjects.addAll(page.items);
        this.notifyDataSetChanged();
    }

    interface EndlessProjectAdapterListener {
        void onLoadMore();
    }
}